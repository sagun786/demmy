import AWSAppSyncClient from 'aws-appsync';
import Amplify, { Auth } from 'aws-amplify';

import appSyncConfig from './appsync';
import awsExports from './aws-exports';

Amplify.configure(awsExports);


export const client = new AWSAppSyncClient({
    url: appSyncConfig.graphqlEndpoint,
    region: appSyncConfig.region,
    disableOffline: true,
    auth: {
        // type: appSyncConfig.authenticationType,
        // apiKey: appSyncConfig.apiKey
        type: 'AWS_IAM',
        credentials: () => Auth.currentCredentials(),
        // type: 'AMAZON_COGNITO_USER_POOLS',
        // jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken()
    }
});
