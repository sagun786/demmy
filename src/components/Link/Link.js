import { Link as RouterLink } from '@reach/router';
import styled from 'styled-components';


export const Link = styled(RouterLink)`
    text-decoration: none;
`;
