import React, { Component } from 'react';
import { Router } from '@reach/router';
import './App.css';

import { CreateGame } from 'components/pages/CreateGame';
import { Game } from 'components/pages/Game';
import { Characters } from 'components/pages/Characters';
import { UserProvider } from 'components/UserProvider';
import { NotificationProvider } from 'components/NotificationProvider';
import { Games } from 'components/pages/Games';
import { Home } from 'components/pages/Home';
import { Header } from 'components/Header';
import { Grid } from './Grid';
import { Body } from './Body';


export class App extends Component {
    render() {
        return (
            <Grid>
                <UserProvider>
                    <NotificationProvider>
                        <Router>
                            <Header path='/*'/>
                        </Router>
                        <Body>
                            <Router style={{ height: '100%'}}>
                                <CreateGame path='/create-game'/>
                                <Games path='/games'/>
                                <Characters path='/characters' />
                                <Game path='/:game'/>
                                <Home path='/'/>
                            </Router>
                        </Body>
                    </NotificationProvider>
                </UserProvider>
            </Grid>
        );
    }
}
