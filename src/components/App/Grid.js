import styled from 'styled-components';


export const Grid = styled.div`
    display: grid;
    height: 100%;

    grid-template-columns: auto;
    grid-template-rows: 7% auto;
`;