import React, { Fragment } from 'react';

import { UserConsumer } from 'components/UserProvider';


export const UserBoundary = ({ children, renderBoundary }) => (
    <UserConsumer>
    {({ userSet }) => (
        <Fragment>
            { !userSet && renderBoundary && renderBoundary() }
            { userSet && children }
        </Fragment>
    )}
    </UserConsumer>
);