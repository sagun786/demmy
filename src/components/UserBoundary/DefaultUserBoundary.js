import React from 'react';

import { UserBoundary } from 'components/UserBoundary';
import { GridTemplate } from 'components/GridTemplate';
import { LoginButton } from 'components/LoginButton';
import { UserConsumer } from 'components/UserProvider';


const UserBoundaryGrid = GridTemplate.extend.attrs({
    template: '100% / 100%',
    justify: 'center',
    align: 'center'
})`
    text-align: center;
`;

export const DefaultUserBoundary = (props) => (
    <UserBoundary
        renderBoundary={() => (
            <UserConsumer>
            {({ signIn }) => (
                <UserBoundaryGrid>
                    <span>
                        <LoginButton onSuccess={ signIn } onFailure={ () => null }>
                            Login to continue
                        </LoginButton>
                    </span>
                </UserBoundaryGrid>
            ) }
            </UserConsumer>
        )}
        { ...props }
    />
);