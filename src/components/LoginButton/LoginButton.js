import React from 'react';
import styled from 'styled-components';
import { GoogleLogin } from 'react-google-login';

const LoginText = styled.div`
    font-size: 1em;
    text-transform: uppercase;
    letter-spacing: .1rem;
    font-weight: 600;
    color: #555;
    &:hover {
            color: #000;
            opacity: 1;
            transition: opacity 0.2s;
        }
        &:before {
            
            height: 15px;
            width: ${({ active }) => active ? '100%' : '0px'};
            left: 0;
            opacity: ${({ active }) => active ? '1' : '0'};
            margin: auto;
            content: ' ';
            position: absolute;
            ${({ active }) => active ? 'box-shadow: 0 4px 0px -2px black' : ''};
        }
        &:hover:before {
            opacity: 1;
            width: 100%;
            box-shadow: 0 4px 0px -2px black;
            transition: width .1s;
        }
`;


const GoogleLoginWithText = ({ children, ...rest }) => <GoogleLogin buttonText={ <LoginText>{ children }</LoginText> } { ...rest } />

export const LoginButton = styled(GoogleLoginWithText)`
    background: none;
    border: none;
    cursor: pointer;
    margin-right: 20px;
    
    padding: 5px;
    
    position: relative;
`;