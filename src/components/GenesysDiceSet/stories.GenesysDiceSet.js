import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';
import { GenesysDiceSet } from './GenesysDiceSet';


storiesOf('Dice Set', module)
    .addDecorator(checkA11y)
    .add('Genesys', () => (
        <Fragment>
            <GenesysDiceSet />
        </Fragment>
    ))