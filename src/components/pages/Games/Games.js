import React, { Component, Fragment } from 'react';
import { Query } from 'react-apollo';

import { Link } from 'components/Link';
import { GamesPageLayout } from './presentational/GamesPageLayout';
import { GamesListLayout } from './presentational/GamesListLayout';
import { GamesHeading } from './presentational/GamesHeading';
import { Game } from './presentational/Game';
import { AddButton } from './presentational/AddButton';
import { GamesQuery } from './Games.gql'
import { DefaultUserBoundary } from 'components/UserBoundary';



export class Games extends Component {

    render() {
        return (
            <DefaultUserBoundary>
                <Query query={ GamesQuery }>
                { ({ loading, error, data }) => {
                    return (
                        <Fragment>
                            { !loading && !error && (
                                <Fragment>
                                    <GamesPageLayout>
                                        <GamesHeading>
                                            Games
                                        </GamesHeading>
                                        <GamesListLayout>
                                            { data.listGames.items.map(game => (
                                                <Game key={ game.id } { ...game } />
                                            )) }
                                        </GamesListLayout>
                                    </GamesPageLayout>
                                    <Link to='/create-game'>
                                        <AddButton/>
                                    </Link>
                                </Fragment>
                            ) }
                        </Fragment>
                    ) }
                }
                </Query>
            </DefaultUserBoundary>
        );
    }
}
