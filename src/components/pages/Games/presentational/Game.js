import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { Link } from 'components/Link';
import { AvatarRow } from './Avatar';
import { GameOptionButton } from '../GameOptionButton';
import { UserAvatar } from 'components/UserAvatar';


export const Game = ({ id, name, creator, members, onDelete }) => (
    <div>
        <Card style={{ position: 'relative' }}>
            <Link to={`/${id}`}>
                <CardMedia
                    style={{ padding: '30% 10%', height: '0'}}
                    image={`https://source.unsplash.com/random/240x320?id=${id}`}
                    title='Placeholder'
                />
            </Link>
            <CardContent>
                <Typography gutterBottom variant='headline' component='h2'>
                    { name }
                </Typography>

                <GameOptionButton gameId={ id } gameCreator={ creator } onDelete={ onDelete }/>

                <AvatarRow>
                    <UserAvatar id={ creator } />

                    { members.map(member => (
                        <UserAvatar id={ member } key={`${ id }-${ member }`} />
                    ))}
                </AvatarRow>
            </CardContent>
        </Card>
    </div>
);
