import React from 'react';
import MaterialAvatar from '@material-ui/core/Avatar';
import styled from 'styled-components';


export const UnstyledAvatar = styled(({ first, ...rest }) => <MaterialAvatar { ...rest }/>)``;


export const AvatarRow = styled.div`
    display: flex;
`;
export const Avatar = UnstyledAvatar.extend`
    margin-left: ${({ first }) => first ? '' : '-15px'};
    border: 3px solid white;
`;
