import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import copy from 'clipboard-copy';

import { NotificationConsumer } from 'components/NotificationProvider';


export class InviteOption extends Component {
    handleClick = () => copy(`https://demmy.heathercreech.me/${ this.props.gameId }`);

    render() {
        return (
            <NotificationConsumer>
            {({ notify }) => (
                <ListItem button onClick={ () => {
                    this.handleClick();
                    notify('Invite link copied!');
                    this.props.onClick();
                }}>
                    <ListItemText primary='Invite' />
                </ListItem>
            ) }
            </NotificationConsumer>
        )
    }
}