import React from 'react';
import { GridTemplate } from 'components/GridTemplate';


const GamesGridTemplate = GridTemplate.extend`
    grid-gap: 20px;
    padding: 20px 0;
    justify-content: space-between;
`;
export const GamesListLayout = (props) => <GamesGridTemplate
    small='auto / 100%'
    large='auto / repeat(3, 30%)'
    { ...props }
/>
