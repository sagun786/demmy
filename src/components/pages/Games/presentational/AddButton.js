import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';


const BottomRightButton = styled(Button)`
    position: fixed!important;
    bottom: 40px;
    right: 40px;
`;

export const AddButton = (props) => (
    <BottomRightButton variant='fab' color='secondary' { ...props }>
        <AddIcon/>
    </BottomRightButton>
);
