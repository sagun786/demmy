import styled from 'styled-components';


export const Cell = styled.p`
    margin: 0;
    padding: 10px;
    text-align: ${({ children }) => isNaN(children) ? 'left' : 'right' };
`;