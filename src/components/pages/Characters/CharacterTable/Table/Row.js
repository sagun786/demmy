import { GridTemplate } from 'components/GridTemplate';

export const Row = GridTemplate.extend.attrs({
    template: 'auto / 3fr 1fr 1fr 1fr 1fr 1fr 1fr'
})`
    grid-gap: 40px;
    height: auto;
    cursor: pointer;
    &:hover {
        background: whitesmoke;
    }
`;