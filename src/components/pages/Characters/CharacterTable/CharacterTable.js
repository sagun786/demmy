import React from 'react';
import { CharactersQuery } from './Characters.gql';
import { Table, TableHeading, Row, Cell } from './Table';
import { Query } from 'react-apollo';
import { CharacterRow } from './CharacterRow';


export const CharacterTable = () => (
    <Query query={ CharactersQuery }>
    {({ loading, error, data }) => {
        let characters = !loading && !error && data && data.listCharacters ? [ ...data.listCharacters.items ] : [];
        characters.sort((a, b) => a.name > b.name);
        return (
            <Table>
                <Row>
                    <TableHeading align='left'>Name</TableHeading>
                    <TableHeading align='right'>Brawn</TableHeading>
                    <TableHeading align='right'>Agility</TableHeading>
                    <TableHeading align='right'>Intellect</TableHeading>
                    <TableHeading align='right'>Cunning</TableHeading>
                    <TableHeading align='right'>Willpower</TableHeading>
                    <TableHeading align='right'>Presence</TableHeading>
                </Row>
                { !loading && !error && characters.map(
                    ({ id, name, characteristics, ...rest }) => (
                        <CharacterRow key={ id } character={{ id, name, characteristics, ...rest }}>
                            <Cell>
                                { name }
                            </Cell>
                            <Cell>{ characteristics.brawn }</Cell>
                            <Cell>{ characteristics.agility }</Cell>
                            <Cell>{ characteristics.intellect }</Cell>
                            <Cell>{ characteristics.cunning }</Cell>
                            <Cell>{ characteristics.willpower }</Cell>
                            <Cell>{ characteristics.presence }</Cell>
                        </CharacterRow>
                    )
                ) }
            </Table>
        )
    }}
    </Query>
);
