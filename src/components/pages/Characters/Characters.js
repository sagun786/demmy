import React from 'react';

import { DefaultUserBoundary } from 'components/UserBoundary';

import { CharactersLayout } from './CharactersLayout';
import { CharacterTable } from './CharacterTable';
import { CreateCharacterDialog } from './CreateCharacterDialog';


export const Characters = () => (
    <DefaultUserBoundary>
        <CharactersLayout>
            <h1 style={{ textAlign: 'center' }}>Characters</h1>
            <CharacterTable />
            <CreateCharacterDialog />
        </CharactersLayout>
    </DefaultUserBoundary>
);