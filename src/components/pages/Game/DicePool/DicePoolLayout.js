import { GridTemplate } from 'components/GridTemplate';


export const DicePoolLayout = GridTemplate.extend.attrs({
    template: 'auto / repeat(6, 50px) 100px 100px'
})`
    height: auto;
    text-align: center;
    padding: 20px 0;
    @media (min-width:768px) {
        justify-content: space-between;
    }
    @media (max-width:768px) {
        align-self: end;
    }
`;
