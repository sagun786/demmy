import { GridTemplate } from 'components/GridTemplate';


export const GameLayout = GridTemplate.extend.attrs({
    small: '50% 50% / auto',
    template: 'auto / 2fr 1fr'
})`
    height: 100%;
    @media (min-width:768px) {
        align-items: center;
    }
`;
