import React, { Component, Fragment } from 'react';
//import styled from 'styled-components';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { UserConsumer } from 'components/UserProvider';
import { UnstyledAvatar } from 'components/pages/Games/presentational/Avatar';


const HeaderAvatar = UnstyledAvatar.extend`
    opacity: .95;
    transition: opacity .2s;
    &:hover {
        opacity: 1;
    }
`;

const LogoutOption = (props) => (
    <UserConsumer>
    {({ signOut }) => (
        <ListItem button { ...props }>
            <ListItemText primary='Logout' color='danger'/>
        </ListItem>
    ) }
    </UserConsumer>
);


export class AccountButton extends Component {
    state = { open: false, el: null }

    handleOpen = () => this.setState({ open: true })
    handleClose = () => this.setState({ open: false, el: null })
    handleClick = e => this.setState({ open: true, el: e.currentTarget })
    
    render() {
        let { open, el } = this.state;
        return (
            <UserConsumer>
            {({ user, image, signOut }) => (
                <Fragment>
                    <IconButton style={{ marginRight: '10px' }} onClick={ this.handleClick }>
                        { image && <HeaderAvatar src={ image } alt={ user }/> }
                        { !image && <AccountCircle title={ user }/> }
                        
                    </IconButton>
                    <Popover
                        open={ open }
                        onClose={ this.handleClose }
                        anchorEl={ el }
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center'}}
                        transformOrigin={{ vertical: 'top', horizontal: 'center'}}
                    >
                        <List>
                            <LogoutOption onClick={ signOut }/>
                        </List>
                    </Popover>
                </Fragment>
            ) }
            </UserConsumer>
        );
    }
}