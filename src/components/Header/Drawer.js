import React from 'react';
// import { withRouter } from 'react-router';
import { navigate } from '@reach/router'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import styled from 'styled-components';


const DrawerWrapper = styled.div`
    width: 250px;
`;

export const Drawer = ({ onOpen, onClose, open, history }) => (
    <SwipeableDrawer open={ open } onOpen={ onOpen } onClose={ onClose }>
        <DrawerWrapper
            tabIndex={ 0 }
            role='button'
            onClick={ onClose }
            onKeyDown={ onClose }
        >
            <List>
                <ListItem button onClick={ () => navigate('/') }>
                    <ListItemText>Home</ListItemText>
                </ListItem>
                <Divider/>
                <ListItem button onClick={ () => navigate('/games') }>
                    <ListItemText>Games</ListItemText>
                </ListItem>
                <ListItem button onClick={ () => navigate('/characters') }>
                    <ListItemText>Characters</ListItemText>
                </ListItem>
            </List>
        </DrawerWrapper>
    </SwipeableDrawer>
);
