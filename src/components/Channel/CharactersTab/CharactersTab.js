import React from 'react';
import { Query } from 'react-apollo';
import { Match } from '@reach/router'
import { ObjectValue } from 'react-values';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import styled from 'styled-components';
import { Value } from 'slate';

import { Tab } from 'components/Tabs';
import { ToolsetTabHeading, ToolsetTabContent } from 'components/GameTools/ToolsetTab';
import { CharactersQuery } from './Characters.gql';
//import { CharacterDialog } from '../../CharacterDialog/CharacterDialog';
import { GridTemplate } from 'components/GridTemplate';
import { Avatar } from 'components/pages/Games/presentational/Avatar';
import { TextEditor } from 'components/TextEditor';


const CharacterListItem = ({ id, children, ...rest }) => (
    <ListItem { ...rest } button>
        <Avatar
            alt=''
            title=''
            src={`https://api.adorable.io/avatars/285/${ id }`}
        />
        <ListItemText>
            { children }
        </ListItemText>
    </ListItem>
);

const ListWrapper = styled.div`
    height: 100%;
    @keyframes fadeIn {
        from { opacity: 0 }
        to { opacity: 1 }
    }

    animation-name: fadeIn;
    animation-timing-function: ease-in;
    animation-duration: .1s;
`;
const CharacterList = ({ set }) => (
    <ListWrapper>
    <List style={{ padding: '0', height: '100%' }}>
        <Match path='/:game'>
        {({ match }) => match && (
            <Query query={ CharactersQuery } variables={{ game: match.game }}>
            {({ loading, error, data }) => !loading && !error && data.listCharacters.items.map(
                character => (
                    <CharacterListItem
                        key={ character.id }
                        onClick={ () => set(character) }
                        id={ character.id }
                    >
                        { character.name }
                    </CharacterListItem>
                )
            )}
            </Query>
        ) }
        </Match>
    </List>
    </ListWrapper>
);


const CharacterHeader = ({ onBack, heading }) => (
    <GridTemplate template='100% / 100px auto 100px' align='center' style={{ marginBottom: '20px' }}>
        <IconButton
            onClick={ onBack }
            arial-label='Back'
        >
            <ChevronLeftIcon />
        </IconButton>
        <h3 style={{ textAlign: 'center' }}>{ heading }</h3>
    </GridTemplate>
);

const Wrapper = styled.div`
    @keyframes fadeIn {
        from { opacity: 0 }
        to { opacity: 1 }
    }

    animation-name: fadeIn;
    animation-timing-function: ease-in;
    animation-duration: .1s;
`;


const Skill = styled.div`
    cursor: pointer;
    margin-bottom: 10px;
    &:hover {
        color: black;
        font-weight: 600;
    }
`;


const parse = json => {
    try {
        return Value.fromJSON(JSON.parse(json));
    } catch(e) {
        return Value.fromJSON({
            document: {
                nodes: [
                    {
                        object: 'block',
                        type: 'paragraph',
                        nodes: [
                            {
                                object: 'text',
                                leaves: [
                                    { text: 'Hello World' }
                                ]
                            }
                        ]
                    }
                ]
            }
        })
    }
}

const SelectedCharacter = ({ unselect, character, setDice }) => (
    <Wrapper>
    <GridTemplate template='40px auto auto / auto' style={{ padding: '0 20px' }}>
        <CharacterHeader onBack={ unselect } heading={ character.name } />
        <TextEditor
            value={ parse(character.bio) }
            readOnly={ true }
        />
        <div style={{ marginTop: '20px' }}>
            <GridTemplate template='repeat(auto-fit, minmax(20px, max-content)) / auto auto auto'>
                { character.skills.map(skill => (
                    <Skill key={ skill.name } onClick={ () => setDice({
                        green: skill.rank > character.characteristics[skill.characteristic] ? skill.rank : character.characteristics[skill.characteristic],
                        yellow: skill.rank > character.characteristics[skill.characteristic] ? character.characteristics[skill.characteristic] : skill.rank
                    })}>
                        { skill.name } - { skill.rank }
                    </Skill>
                )) }
            </GridTemplate>
        </div>
    </GridTemplate>
    </Wrapper>
);


export const CharactersTab = ({ setDice }) => (
    <Tab name='characters'>
        <ToolsetTabHeading>
            Characters
        </ToolsetTabHeading>
        <ToolsetTabContent>
            <ObjectValue>
            { ({ value: character, assign, reset }) => character.id
                    ? ( <SelectedCharacter setDice={ setDice } unselect={ reset } character={ character } /> )
                : ( <CharacterList set={ assign }/> )
            }
            </ObjectValue>
        </ToolsetTabContent>
    </Tab>
);
