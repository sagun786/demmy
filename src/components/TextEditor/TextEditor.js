import React, { Component } from 'react';
import { Editor } from 'slate-react';


/*
const initialValue = Value.fromJSON({
    document: {
        nodes: [
            {
                object: 'block',
                type: 'paragraph',
                nodes: [
                    {
                        object: 'text',
                        leaves: [
                            {text: 'TEXT!'}
                        ]
                    }
                ]
            }
        ]
    }
})
*/

const CodeNode = props => (
    <pre { ...props.attributes }>
        <code>{ props.children }</code>
    </pre>
)

const BoldNode = props => (
    <b { ...props.attributes }>{ props.children }</b>
);

export class TextEditor extends Component {
    /*state = { value: initialValue }
    handleChange = ({ value }) => this.setState({ value })
    */
    handleKeyDown = (event, editor, next) => {
        if (event.ctrlKey) {
            switch(event.key) {
                case '`':
                    const isCode = editor.value.blocks.some(block => block.type === 'code')
                    editor.setBlocks(isCode ? 'paragraph' : 'code');
                    event.preventDefault();
                    return true;
                case 'b':
                    const isBold = editor.value.blocks.some(block => block.type === 'bold')
                    editor.setBlocks(isBold ? 'paragraph' : 'bold');
                    event.preventDefault();
                    return true;
                default:
                    return next();
            }
        }
        return next();
    }
    render() {
        return (
            <Editor
                value={ this.props.value }
                onChange={ this.props.onChange }
                onKeyDown={ this.handleKeyDown }
                readOnly={ this.props.readOnly }
                renderNode={ this.renderNode }
            />
        )
    }

    renderNode = (props, editor, next) => {
        switch(props.node.type) {
            case 'code':
                return <CodeNode { ...props } />
            case 'bold':
                return <BoldNode { ...props } />
            default:
                return next()
        }
    }
}
