import React, { Fragment } from 'react';
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

import { BooleanValue, ObjectValue } from 'react-values';
import { FormConsumer } from '../FormProvider';
import { FormLayout } from './FormLayout';
import { CharacteristicsLayout } from './CharacteristicsLayout';
import { Tabs, Tab, TabHeading, TabContent } from 'components/Tabs';
import { TabHeading as StyledTabHeading } from 'components/Tabs/TabHeading';
import { TextEditor } from 'components/TextEditor';
import { CharacteristicFields } from './CharacteristicFields';
import { GamesTab } from './GamesTab';


const Editable = ({ editing, value, children }) => (
    <Fragment>
        { !editing && value }
        { editing && children }
    </Fragment>
)

const EditableField = ({ editing, value, label, ...rest }) => (
    <Editable editing={ editing } value={`${label}: ${value}`}>
        <TextField
            value={ value }
            label={ label }
            { ...rest }
        />
    </Editable>
);


const BioField = () => (
    <FormConsumer>
    {({ editing, handleBioChange, bio }) => (
        <div>
            <TextEditor
                value={ bio }
                onChange={ handleBioChange }
                readOnly={ !editing }
            />
        </div>
    )}
    </FormConsumer>
);

const StrainField = () => (
    <FormConsumer>
    {({ editing, handleStrainThresholdChange, handleStrainChange, strain: { threshold, current }}) => (
        <div>
            <h5 style={{ marginBottom: '1rem' }}>Strain</h5>
            <EditableField
                id='strain-current'
                label='Current'
                editing={ editing }
                value={ current }
                onChange={ handleStrainChange }
            />
            <span style={{ padding: '0 10px' }}/>
            <EditableField
                id='strain-threshold'
                label='Threshold'
                editing={ editing }
                value={ threshold }
                // Not using this right now
                onChange={ handleStrainThresholdChange }
            />
        </div>
    )}
    </FormConsumer>
);

const CharacteristicSelect = props => (
    <select { ...props }>
        <option value='brawn'>
            Brawn
        </option>
        <option value='agility'>
            Agility
        </option>
        <option value='intellect'>
            Intellect
        </option>
        <option value='cunning'>
            Cunning
        </option>
        <option value='willpower'>
            Willpower
        </option>
        <option value='presence'>
            Presence
        </option>
    </select>
);

const SkillRow = styled.div`
    display: flex;
    cursor: ${ ({ editing }) => editing ? 'initial' : 'pointer' };
    padding: 10px 0;
    ${ ({ editing }) => !editing ? `
        &:hover {
            color: darkslategrey;
            font-weight: 600;
        }
    ` : '' }
`;

const SkillArea = () => (
    <FormConsumer>
    {({ editing, skills, characteristics, handleSkillChange, onSkillClick }) => (
        <div style={{ marginTop: '30px' }}>
            {skills.map(
                skill => (
                    <SkillRow key={ skill.name } editing={ editing } onClick={ () => (!editing && onSkillClick) ? onSkillClick(skill, { skills, characteristics }) : null }>
                        <span style={{ marginRight: '10px', width: '30%' }}>{ skill.name }</span>
                        <EditableField
                            name='rank'
                            value={ skill.rank }
                            label='Rank'
                            editing={ editing }
                            onChange={ e => handleSkillChange({ ...skill, rank: parseInt(e.target.value, 10) }) }
                            style={{ width: '20px' }}
                        />
                        <span style={{ marginLeft: '30px', width: '30%'}}>{ skill.characteristic }</span>
                    </SkillRow>
                )
            )}
            <BooleanValue>
            {({ value: alpha, toggle }) => (
                <div>
                    { alpha &&
                        <div>
                            <ObjectValue defaultValue={{ name: '', rank: '', characteristic: ''}}>
                            {({ value: skill, set }) => (
                                <Fragment>
                                    <input
                                        name='name'
                                        placeholder='Name'
                                        value={ skill.name }
                                        onChange={ e => set(e.target.name, e.target.value) }
                                    />
                                    <input
                                        name='rank'
                                        placeholder='Rank'
                                        value={ skill.rank }
                                        onChange={ e => set(e.target.name, e.target.value) }
                                    />
                                    <CharacteristicSelect
                                        name='characteristic'
                                        value={ skill.characteristic }
                                        onChange={ e => set(e.target.name, e.target.value) }
                                    />
                                    <button onClick={ () => handleSkillChange(skill) }>Save</button>
                                </Fragment>
                            )}
                            </ObjectValue>
                        </div>
                    }
                    {/*
                    <button onClick={ toggle }>Add</button>
                    */}
                </div>
            )}
            </BooleanValue>
        </div>
    )}
    </FormConsumer>
);

export const CharacterTabHeading = ({ children }) => (
    <TabHeading>
    {({ active, handleClick }) => (
        <StyledTabHeading active={ active } onClick={ handleClick }>{ children }</StyledTabHeading>
    )}
    </TabHeading>
);

export const Form = () => (
    <Tabs active='general'>
        <Tab name='general'>
            <CharacterTabHeading>General</CharacterTabHeading>
            <TabContent>
                <FormLayout>
                    <StrainField />
                    <BioField />
                    <CharacteristicsLayout>
                        <CharacteristicFields />
                    </CharacteristicsLayout>
                </FormLayout>
            </TabContent>
        </Tab>
        <Tab name='skills'>
            <CharacterTabHeading>Skills</CharacterTabHeading>
            <TabContent>
                <SkillArea />
            </TabContent>
        </Tab>
        <Tab name='games'>
            <CharacterTabHeading>Games</CharacterTabHeading>
            <TabContent>
                <GamesTab />
            </TabContent>
        </Tab>
    </Tabs>
);
