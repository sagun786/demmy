import { GridTemplate } from 'components/GridTemplate';


export const FormLayout = GridTemplate.extend.attrs({
    template: '2fr 2fr / auto',
    gap: '50px'
})`
    margin-top: 40px;
`;