import React from 'react';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import { GamesTabLayout } from './GamesTabLayout';
import { GamesQuery } from 'components/pages/Games/Games.gql';
import { FormConsumer } from '../../FormProvider';


const GameWrapper = styled.div`
    padding: 0px 10px;
`;

const GameOption = ({ active, children, ...rest }) => (
    <GameWrapper>
        <FormControlLabel
          control={
            <Checkbox
              checked={ active }
              value="active"
              { ...rest }
            />
          }
          label={ children }
        />
    </GameWrapper>
)


export const GamesTab = () => (
    <GamesTabLayout>
        <Query query={ GamesQuery }>
        {({ loading, error, data }) => !loading && !error && (
            <FormConsumer>
            {({ games, handleGameChange }) => data.listGames.items.map(
                game => (
                    <GameOption
                        name={ game.id }
                        active={ games.indexOf(game.id) !== -1 }
                        onChange={ handleGameChange }
                        key={`character-dialog-games-tab-${ game.id }`}
                    >
                        { game.name }
                    </GameOption>
                )
            )}
            
            </FormConsumer>
        )}
        </Query>
    </GamesTabLayout>
);