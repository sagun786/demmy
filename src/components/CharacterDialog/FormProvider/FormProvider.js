import React from 'react';


const Context = React.createContext();
export const FormConsumer = Context.Consumer;

export const FormProvider = ({ children, ...rest }) => (
    <Context.Provider value={ rest }>
        { children }
    </Context.Provider>
);