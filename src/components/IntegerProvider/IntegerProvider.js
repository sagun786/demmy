import React, { Component } from 'react';

const Context = React.createContext();
export const IntegerConsumer = Context.Consumer;


export class IntegerProvider extends Component {

    static defaultProps = {
        initialValue: 0,
        value: 0,
        onChange: () => null,
        min: null,
        max: null
    }

    state = {
        value: this.props.value === undefined ? this.props.initialValue : this.props.value
    }

    handleChange = () => this.props.onChange(this.state.value)
    increment = () => {
        let { max } = this.props;
        let { value } = this.state;
        if (max === null || value < max) {
            this.setState(prevState => ({ value: prevState.value + 1 }), this.handleChange);
        }
    }
    decrement = () => {
        let { min } = this.props;
        let { value } = this.state;
        if (min === null || value > min) {
            this.setState(prevState => ({ value: prevState.value - 1 }), this.handleChange);
        }
    }
    reset = () => this.setState({ value: this.props.initialValue })
    set = value => this.setState({ value }, this.handleChange)


    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value && this.state.value !== this.props.value) {
            this.set(this.props.value);
        }
    }

    render() {
        let { increment, decrement, set, reset } = this;
        return (
            <Context.Provider value={{
                    ...this.state,
                    increment,
                    decrement,
                    set,
                    reset
                }}
            >
                { this.props.children }
            </Context.Provider>
        )
    }
}


export const IntegerProviderAndConsumer = ({ children, ...rest }) => (
    <IntegerProvider { ...rest }>
        <IntegerConsumer>
            { children }
        </IntegerConsumer>
    </IntegerProvider>
);