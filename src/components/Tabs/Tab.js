import React from 'react';

const Context = React.createContext();
export const TabNameConsumer = Context.Consumer;


export const TabNameProvider = ({ name, children }) => (
    <Context.Provider value={{ name }}>
        { children }
    </Context.Provider>
)

export default TabNameProvider;
