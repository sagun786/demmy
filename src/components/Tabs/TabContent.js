import React from 'react';

import { TabConsumer } from './TabProvider';
import { TabNameConsumer } from './Tab';
import { TabContentConsumer } from './TabContentProvider';


export default (props) => (
    <TabConsumer>
    {({ activeTab, setActiveTab }) => (
        <TabNameConsumer>
        {({ name }) => name === activeTab && (
            <TabContentConsumer>
            {({ content, updateContent }) => {
                if (content !== props.children) {
                    updateContent(props.children);
                }
            } }
            </TabContentConsumer>
        ) }
        </TabNameConsumer>
    )}
    </TabConsumer>
);
