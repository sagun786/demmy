import React, { Component } from 'react';

const Context = React.createContext();
export const TabConsumer = Context.Consumer;


export class TabProvider extends Component {
    state = { activeTab: this.props.active ? this.props.active : '' };
    setActiveTab = (name) => this.setState({ activeTab: name });
    render() {
        let { activeTab } = this.state;
        let { children } = this.props;
        return (
            <Context.Provider value={{ activeTab, setActiveTab: this.setActiveTab }}>
                { children }
            </Context.Provider>
        );
    }
}
