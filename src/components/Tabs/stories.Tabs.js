import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';
import { Tabs, Tab, TabHeading, TabContent } from '.';
import { TabHeading as StyledTabHeading } from './TabHeading';


const Heading = ({ children }) => (
    <TabHeading>
    {({ active, handleClick }) => (
        <div active={ active } onClick={ handleClick }>{ children }</div>
    )}
    </TabHeading>
);
const StyledHeading = ({ children }) => (
    <TabHeading>
    {({ active, handleClick }) => (
        <StyledTabHeading active={ active } onClick={ handleClick }>{ children }</StyledTabHeading>
    )}
    </TabHeading>
);

const StyledContent = ({ children }) => (
    <TabContent>
        <div style={{ marginTop: '30px' }}>{ children }</div>
    </TabContent>
)

storiesOf('Tabs', module)
    .addDecorator(checkA11y)
    .add('unstyled', () => (
        <Tabs active='settings'>
            <Tab name='chat'>
                <Heading>
                    Chat
                </Heading>
                <TabContent>
                    Chat goes here
                </TabContent>
            </Tab>
            <Tab name='settings'>
                <Heading>
                    Settings
                </Heading>
                <TabContent>
                    Settings panel
                </TabContent>
            </Tab>
        </Tabs>
    ))
    .add('styled', () => (
        <Tabs active='settings'>
            <Tab name='chat'>
                <StyledHeading>
                    Chat
                </StyledHeading>
                <StyledContent>
                    Chat goes here
                </StyledContent>
            </Tab>
            <Tab name='settings'>
                <StyledHeading>
                    Settings
                </StyledHeading>
                <StyledContent>
                    Settings panel
                </StyledContent>
            </Tab>
        </Tabs>
    ))
    