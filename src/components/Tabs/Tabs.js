import React, { Component } from 'react';
import { TabProvider } from './TabProvider';
import { TabContentProvider, TabContentConsumer } from './TabContentProvider';


export class Tabs extends Component {

    state = { content: '' }
    updateContent = content => this.setState({ content })

    render() {
        let { active, children } = this.props;
        return (
            <TabProvider active={ active }>
                <TabContentProvider>
                    { children }
                    <TabContentConsumer>
                    {({ content }) => (
                        content
                    ) }
                    </TabContentConsumer>
                </TabContentProvider>
            </TabProvider>
        );
    }
}
