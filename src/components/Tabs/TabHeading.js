import React from 'react';
import styled from 'styled-components';
import { TabConsumer } from './TabProvider';
import { TabNameConsumer } from './Tab';


export const TabHeading = styled.h4`
    display: inline;
    padding: 20px;
    margin-bottom: 20px;
    opacity: ${({ active }) => active ? '1' : '0.5'};
    cursor: pointer;
    position: relative;
    text-align: center;
    &:hover {
        opacity: 1;
        transition: opacity 0.2s;
    }
    &:before {
        height: 50%;
        width: ${({ active }) => active ? '100%' : '0px'};
        left: 0;
        opacity: ${({ active }) => active ? '1' : '0'};
        margin: auto;
        content: ' ';
        position: absolute;
        ${({ active }) => active ? 'box-shadow: 0 4px 0px -2px black' : ''};
    }
    &:hover:before {
        opacity: 1;
        width: 100%;
        box-shadow: 0 4px 0px -2px black;
        transition: width 0.1s;
    }
`;

export default ({ children }) => (
    <TabConsumer>
    {({ activeTab, setActiveTab }) => (
        <TabNameConsumer>
        {({ name }) => children({
            handleClick: () => setActiveTab(name),
            active: activeTab === name
        }) }
        </TabNameConsumer>
    )}
    </TabConsumer>
);