import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { GridTemplate } from 'components/GridTemplate';

const Token = styled.div`
    background: url(${({ image }) => image}) center/cover;
    border-radius: 50%;
    border: 2px solid lightgrey;
    width: 50px;
    height: 50px;
    cursor: pointer;
    background: ${({active}) => active ? 'lavender' : 'lavenderblush'};
    display: grid;
    align-items: center;
    text-align: center;
    #padding: 5px 10px;
    margin: 10px;
    &:hover {
        background: lavender;
    }
`


const RangeGrid = props => <GridTemplate template='auto / 16.5% 16.5% 16.5% 16.5% 16.5% 16.5%' align='center' { ...props } />;
const RangeGridBox = GridTemplate.extend.attrs({
    template: 'auto / auto auto auto',
    justify: 'center',
    align: 'center'
})`
    height: 100%;
    width: 100%;
    border: ${({ noBorder }) => noBorder ? '' : '1px solid black'};
    padding: 10px 0;
`

const Engaged = styled.div`opacity: 1;`;
const Short = styled.div`opacity: .8;`;
const Medium = styled.div`opacity: .6;`;
const Long = styled.div`opacity: .4;`;
const Extreme = styled.div`opacity: .2;`;

export class RangeMap extends Component {

    state = {
        selected: '2'
    }

    handleClick = id => this.setState({ selected: id })

    render(){
        let { selected } = this.state;
        let { data } = this.props;

        return (
            <div>
                <h2>Genesys Range Visualization</h2>
                { selected && (
                    <div>
                        <RangeGrid>
                            <RangeGridBox noBorder>
                                <Token active={ true }>{ selected }</Token>
                                <span style={{ padding: '20px', fontSize: '20px', fontWeight: '600'}}>=></span>
                            </RangeGridBox>
                            <RangeGridBox>
                                    { data.tokens[selected].engaged.map(id => <Token key={`engaged-${selected}-${id}`}>{ id }</Token>) }
                            </RangeGridBox>
                            <RangeGridBox>
                                { data.tokens[selected].short.map(id => <Token key={`engaged-${selected}-${id}`}>{ id }</Token>) }
                            </RangeGridBox>
                            <RangeGridBox>
                                { data.tokens[selected].medium.map(id => <Token key={`engaged-${selected}-${id}`}>{ id }</Token>) }
                            </RangeGridBox>
                            <RangeGridBox>
                                { data.tokens[selected].long.map(id => <Token key={`engaged-${selected}-${id}`}>{ id }</Token>) }
                            </RangeGridBox>
                            <RangeGridBox>
                                { data.tokens[selected].extreme.map(id => <Token key={`engaged-${selected}-${id}`}>{ id }</Token>) }
                            </RangeGridBox>
                        </RangeGrid>
                    </div>
                ) }
                { !selected && (
                    <div>Nothing is selected</div>
                ) }
                <div style={{ marginTop: '30px'}}>
                    <h4>Tokens</h4>
                    { Object.values(data.tokens).map((token, index) => (
                        <Token onClick={() => this.handleClick(token.id)} active={ selected === token.id }>{ token.id }</Token>
                    ))}
                </div>
            </div>
        )
    }

}