import React from 'react';
import { storiesOf } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';
import { DieWithCount } from '.';


storiesOf('Die', module)
    .addDecorator(checkA11y)
    .add('Die with count', () => {
        return (
            <DieWithCount
                increment={ () => console.log('+ 1') }
                decrement={ () => console.log('- 1') }
                value={ 0 }
                color={ 'green' }
            />
        )
    })
