import { createMuiTheme } from '@material-ui/core/styles';
//import black from '@material-ui/core/colors/black';


export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#fafafa',
            light: '#ffffff',
            dark: '#c7c7c7',
            contrastText: 'black'
        },
        secondary: {
            main: '#d7ccc8',
            light: '#fffffb',
            dark: '#a69b97',
            contrastText: 'black'
        }
        /*
        primary: {
            main: '#37474f',
            light: '#62727b',
            dark: '#102027',
            contrastText: 'white'
        },
        secondary: {
            main: '#7e57c2',
            light: '#b085f5',
            dark: '#4d2c91',
            contrastText: 'white'
        }
        /*
        primary: {
            main: '#02a652',
            light: '#55d980',
            dark: '#007627',
            contrastText: 'white'
        }*/
    }
});


export const colors = {
    accent: '#ef6c1a',
    superAccent: '#cae7f2'
};
